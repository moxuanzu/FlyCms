package com.flycms.modules.applet.domain;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 小程序开发者ID设置对象 fly_app_config
 * 
 * @author admin
 * @date 2020-05-27
 */
@Data
public class AppConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 平台名称 */
    @Excel(name = "平台名称")
    private String platform;
    /** AppId */
    @Excel(name = "AppId")
    private String appId;
    /** AppSecret */
    private String appSecret;
    /** 申请网址 */
    @Excel(name = "申请网址")
    private String platformUrl;
}
