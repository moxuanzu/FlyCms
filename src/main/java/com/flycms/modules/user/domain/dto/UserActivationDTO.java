package com.flycms.modules.user.domain.dto;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 注册码数据传输对象 fly_user_activation
 * 
 * @author admin
 * @date 2020-12-08
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class UserActivationDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 账号 */
    @Excel(name = "账号")
    private String userName;
    /** 验证码 */
    @Excel(name = "验证码")
    private String code;
    /** 类型 */
    @Excel(name = "类型")
    private Integer codeType;
    /** 时间 */
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date referTime;
    /** 状态 */
    @Excel(name = "状态")
    private Integer referStatus;

}
