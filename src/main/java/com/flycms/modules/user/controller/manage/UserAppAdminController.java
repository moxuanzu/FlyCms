package com.flycms.modules.user.controller.manage;

import com.flycms.modules.user.service.IUserAppService;
import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.user.domain.UserApp;
import com.flycms.modules.user.domain.dto.UserAppDto;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 用户用小程序注册信息Controller
 * 
 * @author admin
 * @date 2020-05-30
 */
@RestController
@RequestMapping("/system/user/userApp")
public class UserAppAdminController extends BaseController
{
    @Autowired
    private IUserAppService userAppService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增用户用小程序注册信息
     */
    @PreAuthorize("@ss.hasPermi('user:userApp:add')")
    @Log(title = "用户用小程序注册信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserApp userApp)
    {
        if (UserConstants.NOT_UNIQUE.equals(userAppService.checkUserAppOpenIdUnique(userApp)))
        {
            return AjaxResult.error("新增用户用小程序注册信息'" + userApp.getOpenId() + "'失败，openid已存在");
        }
        if(StringUtils.isEmpty(userApp.getUnionId())){
            if (UserConstants.NOT_UNIQUE.equals(userAppService.checkUserAppUnionIdUnique(userApp)))
            {
                return AjaxResult.error("新增用户用小程序注册信息'" + userApp.getUnionId() + "'失败，unionid已存在");
            }
        }

        return toAjax(userAppService.insertUserApp(userApp));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户用小程序注册信息
     */
    @PreAuthorize("@ss.hasPermi('user:userApp:remove')")
    @Log(title = "用户用小程序注册信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(userAppService.deleteUserAppByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户用小程序注册信息
     */
    @PreAuthorize("@ss.hasPermi('user:userApp:edit')")
    @Log(title = "用户用小程序注册信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserApp userApp)
    {
        return toAjax(userAppService.updateUserApp(userApp));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询用户用小程序注册信息列表
     */
    @PreAuthorize("@ss.hasPermi('user:userApp:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserApp userApp,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "create_time") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<UserAppDto> pager = userAppService.selectUserAppPager(userApp, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出用户用小程序注册信息列表
     */
    @PreAuthorize("@ss.hasPermi('user:userApp:export')")
    @Log(title = "用户用小程序注册信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserApp userApp,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<UserAppDto> pager = userAppService.selectUserAppPager(userApp, pageNum, pageSize, sort, order);
        ExcelUtil<UserAppDto> util = new ExcelUtil<UserAppDto>(UserAppDto.class);
        return util.exportExcel(pager.getList(), "userApp");
    }

    /**
     * 获取用户用小程序注册信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('user:userApp:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(userAppService.selectUserAppById(id));
    }

}
