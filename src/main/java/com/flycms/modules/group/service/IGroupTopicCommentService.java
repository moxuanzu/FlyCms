package com.flycms.modules.group.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupTopicComment;
import com.flycms.modules.group.domain.dto.GroupTopicCommentDTO;

import java.util.List;

/**
 * 话题回复/评论Service接口
 * 
 * @author admin
 * @date 2020-12-15
 */
public interface IGroupTopicCommentService
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    public int insertFlyGroupTopicComment(GroupTopicComment groupTopicComment);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除话题回复/评论
     *
     * @param ids 需要删除的话题回复/评论ID
     * @return 结果
     */
    public int deleteFlyGroupTopicCommentByIds(Long[] ids);

    /**
     * 删除话题回复/评论信息
     *
     * @param id 话题回复/评论ID
     * @return 结果
     */
    public int deleteFlyGroupTopicCommentById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    public int updateFlyGroupTopicComment(GroupTopicComment groupTopicComment);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询话题回复/评论
     * 
     * @param id 话题回复/评论ID
     * @return 话题回复/评论
     */
    public GroupTopicCommentDTO findFlyGroupTopicCommentById(Long id);

    /**
     * 查询话题回复/评论列表
     * 
     * @param groupTopicComment 话题回复/评论
     * @return 话题回复/评论集合
     */
    public Pager<GroupTopicCommentDTO> selectFlyGroupTopicCommentPager(GroupTopicComment groupTopicComment, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的话题回复/评论列表
     *
     * @param groupTopicComment 话题回复/评论
     * @return 话题回复/评论集合
     */
    public List<GroupTopicCommentDTO> exportFlyGroupTopicCommentList(GroupTopicComment groupTopicComment);
}
