package com.flycms.modules.group.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupTopicRecommend;
import com.flycms.modules.group.domain.dto.GroupTopicRecommendDTO;

import java.util.List;

/**
 * 内容推荐Service接口
 * 
 * @author admin
 * @date 2020-12-07
 */
public interface IGroupTopicRecommendService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增内容推荐
     *
     * @param groupTopicRecommend 内容推荐
     * @return 结果
     */
    public int insertGroupTopicRecommend(GroupTopicRecommend groupTopicRecommend);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除内容推荐
     *
     * @param ids 需要删除的内容推荐ID
     * @return 结果
     */
    public int deleteGroupTopicRecommendByIds(Long[] ids);

    /**
     * 删除内容推荐信息
     *
     * @param id 内容推荐ID
     * @return 结果
     */
    public int deleteGroupTopicRecommendById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改内容推荐
     *
     * @param groupTopicRecommend 内容推荐
     * @return 结果
     */
    public int updateGroupTopicRecommend(GroupTopicRecommend groupTopicRecommend);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验内容推荐关联是否唯一
     *
     * @param groupTopicRecommend 小组话题
     * @return 结果
     */
    public String checkGroupTopicRecommendUnique(GroupTopicRecommend groupTopicRecommend);
    /**
     * 查询内容推荐
     * 
     * @param id 内容推荐ID
     * @return 内容推荐
     */
    public GroupTopicRecommendDTO findGroupTopicRecommendById(Long id);

    /**
     * 查询内容推荐列表
     * 
     * @param groupTopicRecommend 内容推荐
     * @return 内容推荐集合
     */
    public Pager<GroupTopicRecommendDTO> selectGroupTopicRecommendPager(GroupTopicRecommend groupTopicRecommend, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的内容推荐列表
     *
     * @param groupTopicRecommend 内容推荐
     * @return 内容推荐集合
     */
    public List<GroupTopicRecommendDTO> exportGroupTopicRecommendList(GroupTopicRecommend groupTopicRecommend);
}
