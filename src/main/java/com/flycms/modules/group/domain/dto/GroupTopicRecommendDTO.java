package com.flycms.modules.group.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 内容推荐数据传输对象 fly_group_topic_recommend
 * 
 * @author admin
 * @date 2020-12-07
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class GroupTopicRecommendDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** id */
    @Excel(name = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 小组ID */
    @Excel(name = "小组ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long groupId;
    /** 内容类型id */
    @Excel(name = "内容类型id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long contentTypeId;
    /** 内容id */
    @Excel(name = "内容id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long contentId;
    /** 推荐设置 */
    @Excel(name = "推荐设置")
    private Integer recommend;
    /** 设置管理员 */
    @Excel(name = "设置管理员")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long adminId;

}
