package com.flycms.modules.group.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 话题回复/评论数据传输对象 fly_group_topic_comment
 * 
 * @author admin
 * @date 2020-12-15
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class GroupTopicCommentDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 自增评论ID */
    @Excel(name = "自增评论ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 上级评论ID */
    @Excel(name = "上级评论ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long referId;
    /** 话题ID */
    @Excel(name = "话题ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long topicId;
    /** 用户ID */
    @Excel(name = "用户ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    /** 回复内容 */
    @Excel(name = "回复内容")
    private String content;
    /** 0公开1不公开（仅自己和发帖者可看） */
    @Excel(name = "0公开1不公开", readConverterExp = "仅=自己和发帖者可看")
    private Integer ispublic;

}
