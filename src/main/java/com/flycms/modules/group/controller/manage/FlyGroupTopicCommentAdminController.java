package com.flycms.modules.group.controller.manage;


import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.group.domain.GroupTopicComment;
import com.flycms.modules.group.domain.dto.GroupTopicCommentDTO;
import com.flycms.modules.group.service.IGroupTopicCommentService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 话题回复/评论Controller
 * 
 * @author admin
 * @date 2020-12-15
 */
@RestController
@RequestMapping("/system/group/comment")
public class FlyGroupTopicCommentAdminController extends BaseController
{
    @Autowired
    private IGroupTopicCommentService flyGroupTopicCommentService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增话题回复/评论
     */
    @PreAuthorize("@ss.hasPermi('group:comment:add')")
    @Log(title = "话题回复/评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GroupTopicComment groupTopicComment)
    {
        return toAjax(flyGroupTopicCommentService.insertFlyGroupTopicComment(groupTopicComment));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除话题回复/评论
     */
    @PreAuthorize("@ss.hasPermi('group:comment:remove')")
    @Log(title = "话题回复/评论", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(flyGroupTopicCommentService.deleteFlyGroupTopicCommentByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题回复/评论
     */
    @PreAuthorize("@ss.hasPermi('group:comment:edit')")
    @Log(title = "话题回复/评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GroupTopicComment groupTopicComment)
    {
        return toAjax(flyGroupTopicCommentService.updateFlyGroupTopicComment(groupTopicComment));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询话题回复/评论列表
     */
    @PreAuthorize("@ss.hasPermi('group:comment:list')")
    @GetMapping("/list")
    public TableDataInfo list(GroupTopicComment groupTopicComment,
                              @RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @Sort @RequestParam(defaultValue = "id") String sort,
                              @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupTopicCommentDTO> pager = flyGroupTopicCommentService.selectFlyGroupTopicCommentPager(groupTopicComment, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出话题回复/评论列表
     */
    @PreAuthorize("@ss.hasPermi('group:comment:export')")
    @Log(title = "话题回复/评论", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(GroupTopicComment groupTopicComment)
    {
        List<GroupTopicCommentDTO> flyGroupTopicCommentList = flyGroupTopicCommentService.exportFlyGroupTopicCommentList(groupTopicComment);
        ExcelUtil<GroupTopicCommentDTO> util = new ExcelUtil<GroupTopicCommentDTO>(GroupTopicCommentDTO.class);
        return util.exportExcel(flyGroupTopicCommentList, "comment");
    }

    /**
     * 获取话题回复/评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('group:comment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(flyGroupTopicCommentService.findFlyGroupTopicCommentById(id));
    }

}
