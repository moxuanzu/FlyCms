package com.flycms.modules.group.controller.manage;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import com.flycms.modules.elastic.service.IElasticSearchService;
import com.flycms.modules.group.domain.GroupTopicRecommend;
import com.flycms.modules.group.domain.vo.GroupTopicVO;
import com.flycms.modules.group.service.IGroupTopicRecommendService;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import com.flycms.modules.group.service.IGroupTopicService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 小组话题Controller
 * 
 * @author admin
 * @date 2020-09-27
 */
@RestController
@RequestMapping("/system/group/topic")
public class GroupTopicAdminController extends BaseController
{
    @Autowired
    private IGroupTopicService groupTopicService;
    @Autowired
    private IElasticSearchService elasticSearchService;
    @Autowired
    private IGroupTopicRecommendService groupTopicRecommendService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增内容推荐关联设置
     */
    @PreAuthorize("@ss.hasPermi('group:recommend:add')")
    @Log(title = "内容推荐", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GroupTopicRecommend groupTopicRecommend)
    {
        if (UserConstants.NOT_UNIQUE.equals(groupTopicRecommendService.checkGroupTopicRecommendUnique(groupTopicRecommend)))
        {
            return AjaxResult.error("已设置");
        }
        return toAjax(groupTopicRecommendService.insertGroupTopicRecommend(groupTopicRecommend));
    }
    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小组话题
     */
    @PreAuthorize("@ss.hasPermi('group:topic:remove')")
    @Log(title = "小组话题", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(groupTopicService.deleteGroupTopicByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组话题
     */
    @PreAuthorize("@ss.hasPermi('group:topic:edit')")
    @Log(title = "小组话题", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GroupTopic groupTopic)
    {
        return toAjax(groupTopicService.updateGroupTopic(groupTopic));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询小组话题列表
     */
    @PreAuthorize("@ss.hasPermi('group:topic:list')")
    @GetMapping("/list")
    public TableDataInfo list(GroupTopic groupTopic,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "create_time") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupTopicDTO> pager = groupTopicService.selectGroupTopicPager(groupTopic, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出小组话题列表
     */
    @PreAuthorize("@ss.hasPermi('group:topic:export')")
    @Log(title = "小组话题", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export()
    {
        List<GroupTopicDTO> topic = groupTopicService.selectGroupTopicAllList();
        ExcelUtil<GroupTopicDTO> util = new ExcelUtil<GroupTopicDTO>(GroupTopicDTO.class);
        return util.exportExcel(topic, "小组话题");
    }

    /**
     * 获取小组话题详细信息
     */
    @PreAuthorize("@ss.hasPermi('group:topic:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(groupTopicService.findGroupTopicById(id));
    }

    /**
     * 获取同义词词库详细信息
     */
    @PreAuthorize("@ss.hasPermi('dream:dream:query')")
    @GetMapping(value = "/all-index")
    public AjaxResult allIndex() throws Exception {
        if (elasticSearchService.instance() != null) {
            elasticSearchService.deleteAllIndex();
            List<GroupTopicDTO> topics = groupTopicService.selectGroupTopicAllList();
            Map<String, Map<String, Object>> sources = topics.stream().collect(Collectors.toMap(key -> String.valueOf(key
                    .getId()), value -> {
                Map<String, Object> map = new HashMap<>();
                map.put("title", value.getTitle());
                map.put("content", value.getContent());
                return map;
            }));
            elasticSearchService.bulkDocument("topic", sources);
            return AjaxResult.success("更新成功");
        }
        return AjaxResult.error("更新失败");
    }
}
