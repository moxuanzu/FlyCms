package com.flycms.modules.notify.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 系统邮件模板设置对象 fly_email_templet
 * 
 * @author admin
 * @date 2020-11-09
 */
@Data
public class EmailTemplet extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 模板代码 */
    private String tpCode;
    /** 邮件标题 */
    private String title;
    /** 邮件内容 */
    private String content;
}
