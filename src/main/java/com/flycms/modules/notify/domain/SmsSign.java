package com.flycms.modules.notify.domain;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 短信签名对象 fly_sms_sign
 * 
 * @author admin
 * @date 2020-05-27
 */
@Data
public class SmsSign extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 接口ID */
    private Long apiId;
    /** 签名名称 */
    private String signName;
    /** 状态 */
    private Integer status;
}
