package com.flycms.modules.notify.mapper;

import com.flycms.common.dao.BaseDao;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.Email;
import com.flycms.modules.notify.domain.SmsApi;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmailMapper extends BaseDao<Email> {

    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增邮件服务器
     *
     * @param email 邮件服务器
     * @return 结果
     */
    public int insertEmail(Email email);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除邮件服务器
     *
     * @param mailServer 邮件服务器ID
     * @return 结果
     */
    public int deleteEmailById(String mailServer);

    /**
     * 批量删除邮件服务器
     *
     * @param mailServers 需要删除的数据ID
     * @return 结果
     */
    public int deleteEmailByIds(String[] mailServers);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改邮件服务器
     *
     * @param email 邮件服务器
     * @return 结果
     */
    public int updateEmail(Email email);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验发件箱KEY是否唯一
     *
     * @param email 邮件服务器ID
     * @return 结果
     */
    public int checkEmailMailServerUnique(Email  email);


    /**
     * 查询邮件服务器
     *
     * @param mailServer 邮件服务器ID
     * @return 邮件服务器
     */
    public Email findEmailByServer(String mailServer);

    /**
     * 查询邮件服务器数量
     *
     * @param pager 分页处理类
     * @return 邮件服务器数量
     */
    public int queryEmailTotal(Pager pager);

    /**
     * 查询邮件服务器列表
     *
     * @param pager 分页处理类
     * @return 邮件服务器集合
     */
    public List<Email> selectEmailPager(Pager pager);

    /**
     * 查询需要导出的邮件服务器列表
     *
     * @param email 邮件服务器
     * @return 邮件服务器集合
     */
    public List<Email> exportEmailList(Email email);
}
