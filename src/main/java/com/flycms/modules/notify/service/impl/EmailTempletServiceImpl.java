package com.flycms.modules.notify.service.impl;


import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.mapper.EmailTempletMapper;
import com.flycms.modules.notify.domain.EmailTemplet;
import com.flycms.modules.notify.domain.dto.EmailTempletDTO;
import com.flycms.modules.notify.service.IEmailTempletService;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统邮件模板设置Service业务层处理
 * 
 * @author admin
 * @date 2020-11-09
 */
@Service
public class EmailTempletServiceImpl implements IEmailTempletService 
{
    @Autowired
    private EmailTempletMapper emailTempletMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增系统邮件模板设置
     *
     * @param emailTemplet 系统邮件模板设置
     * @return 结果
     */
    @Override
    public int insertEmailTemplet(EmailTemplet emailTemplet)
    {
        emailTemplet.setId(SnowFlakeUtils.nextId());
        return emailTempletMapper.insertEmailTemplet(emailTemplet);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除系统邮件模板设置
     *
     * @param ids 需要删除的系统邮件模板设置ID
     * @return 结果
     */
    @Override
    public int deleteEmailTempletByIds(Long[] ids)
    {
        return emailTempletMapper.deleteEmailTempletByIds(ids);
    }

    /**
     * 删除系统邮件模板设置信息
     *
     * @param id 系统邮件模板设置ID
     * @return 结果
     */
    @Override
    public int deleteEmailTempletById(Long id)
    {
        return emailTempletMapper.deleteEmailTempletById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改系统邮件模板设置
     *
     * @param emailTemplet 系统邮件模板设置
     * @return 结果
     */
    @Override
    public int updateEmailTemplet(EmailTemplet emailTemplet)
    {
        return emailTempletMapper.updateEmailTemplet(emailTemplet);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询系统邮件模板设置
     * 
     * @param id 系统邮件模板设置ID
     * @return 系统邮件模板设置
     */
    @Override
    public EmailTempletDTO findEmailTempletById(Long id)
    {
        EmailTemplet  emailTemplet=emailTempletMapper.findEmailTempletById(id);
        return BeanConvertor.convertBean(emailTemplet,EmailTempletDTO.class);
    }


    /**
     * 查询系统邮件模板设置列表
     *
     * @param emailTemplet 系统邮件模板设置
     * @return 系统邮件模板设置
     */
    @Override
    public Pager<EmailTempletDTO> selectEmailTempletPager(EmailTemplet emailTemplet, Integer page, Integer limit, String sort, String order)
    {
        Pager<EmailTempletDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(emailTemplet);

        List<EmailTemplet> emailTempletList=emailTempletMapper.selectEmailTempletPager(pager);
        List<EmailTempletDTO> dtolsit = new ArrayList<EmailTempletDTO>();
        emailTempletList.forEach(entity -> {
            EmailTempletDTO dto = new EmailTempletDTO();
            dto.setId(entity.getId());
            dto.setTpCode(entity.getTpCode());
            dto.setTitle(entity.getTitle());
            dto.setContent(entity.getContent());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(emailTempletMapper.queryEmailTempletTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的系统邮件模板设置列表
     *
     * @param emailTemplet 系统邮件模板设置
     * @return 系统邮件模板设置集合
     */
    @Override
    public List<EmailTempletDTO> exportEmailTempletList(EmailTemplet emailTemplet) {
        return BeanConvertor.copyList(emailTempletMapper.exportEmailTempletList(emailTemplet),EmailTempletDTO.class);
    }
}
