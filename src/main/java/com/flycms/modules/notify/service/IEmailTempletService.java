package com.flycms.modules.notify.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.EmailTemplet;
import com.flycms.modules.notify.domain.dto.EmailTempletDTO;

import java.util.List;

/**
 * 系统邮件模板设置Service接口
 * 
 * @author admin
 * @date 2020-11-09
 */
public interface IEmailTempletService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增系统邮件模板设置
     *
     * @param emailTemplet 系统邮件模板设置
     * @return 结果
     */
    public int insertEmailTemplet(EmailTemplet emailTemplet);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除系统邮件模板设置
     *
     * @param ids 需要删除的系统邮件模板设置ID
     * @return 结果
     */
    public int deleteEmailTempletByIds(Long[] ids);

    /**
     * 删除系统邮件模板设置信息
     *
     * @param id 系统邮件模板设置ID
     * @return 结果
     */
    public int deleteEmailTempletById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改系统邮件模板设置
     *
     * @param emailTemplet 系统邮件模板设置
     * @return 结果
     */
    public int updateEmailTemplet(EmailTemplet emailTemplet);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询系统邮件模板设置
     * 
     * @param id 系统邮件模板设置ID
     * @return 系统邮件模板设置
     */
    public EmailTempletDTO findEmailTempletById(Long id);

    /**
     * 查询系统邮件模板设置列表
     * 
     * @param emailTemplet 系统邮件模板设置
     * @return 系统邮件模板设置集合
     */
    public Pager<EmailTempletDTO> selectEmailTempletPager(EmailTemplet emailTemplet, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的系统邮件模板设置列表
     *
     * @param emailTemplet 系统邮件模板设置
     * @return 系统邮件模板设置集合
     */
    public List<EmailTempletDTO> exportEmailTempletList(EmailTemplet emailTemplet);
}
