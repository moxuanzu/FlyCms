package com.flycms.modules.notify.service.impl;

import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.SmsCode;
import com.flycms.modules.notify.mapper.SmsCodeMapper;
import com.flycms.modules.notify.service.ISmsCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.modules.notify.domain.dto.SmsCodeDto;

import java.util.ArrayList;
import java.util.List;

/**
 * 短信验证码Service业务层处理
 * 
 * @author kaifei sun
 * @date 2020-05-27
 */
@Service
public class SmsCodeServiceImpl implements ISmsCodeService
{
    @Autowired
    private SmsCodeMapper smsCodeMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信验证码
     *
     * @param smsCode 短信验证码
     * @return 结果
     */
    @Override
    public int insertSmsCode(SmsCode smsCode)
    {
        smsCode.setId(SnowFlakeUtils.nextId());
        smsCode.setCreateTime(DateUtils.getNowDate());
        return smsCodeMapper.insertSmsCode(smsCode);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除短信验证码
     *
     * @param ids 需要删除的短信验证码ID
     * @return 结果
     */
    @Override
    public int deleteSmsCodeByIds(Long[] ids)
    {
        return smsCodeMapper.deleteSmsCodeByIds(ids);
    }

    /**
     * 删除短信验证码信息
     *
     * @param id 短信验证码ID
     * @return 结果
     */
    @Override
    public int deleteSmsCodeById(Long id)
    {
        return smsCodeMapper.deleteSmsCodeById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信验证码
     *
     * @param smsCode 短信验证码
     * @return 结果
     */
    @Override
    public int updateSmsCode(SmsCode smsCode)
    {
        return smsCodeMapper.updateSmsCode(smsCode);
    }

    /**
     * 按用户名（邮箱、手机号）+ 验证码查询修改验证状态为已验证，0未验证，1为已验证
     *
     * @param userName  按用户名（邮箱、手机号）
     * @param code  验证码
     * @return
     */
    @Override
    public int updateSmsCodeByStatus(String userName,String code)
    {
        return smsCodeMapper.updateSmsCodeByStatus(userName,code);
    }
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询验证码在当前时间5分钟内获取并且是否过时或不存在
     *
     * @param userName
     *         查询的用户名
     * @param codeType
     *         查询的验证码类型，1手机注册验证码,2安全手机设置验证码,3密码重置验证码
     * @param code
     *         验证码
     * @return
     */
    public boolean checkSmsCodeByCode(Long userId, String userName, Integer codeType, String code){
        SmsCode activation = smsCodeMapper.findSmsCodeByCode(userId, userName, codeType);
        if(activation!=null){
            if(activation.getCode().equals(code)){
                return true;
            }
        }
        return false;
    }

    /**
     * 查询指定日期内申请验证码次数
     *
     * @param userId
     * @param userName
     * @param createTime
     * @return
     */
    @Override
    public int findSmsCodeCount(Long userId, String userName, String createTime)
    {
        return smsCodeMapper.findSmsCodeCount(userId, userName, createTime);
    }

    /**
     * 查询验证码在当前时间5分钟内获取并且是否过时或不存在
     *
     * @param userId
     * @param userName
     * @param codeType 注册码类型：1手机注册验证码,2安全手机设置验证码,3密码重置验证码
     * @return
     */
    public SmsCode findSmsCodeByCode(Long userId, String userName, Integer codeType)
    {
        return smsCodeMapper.findSmsCodeByCode(userId, userName, codeType);
    }

    /**
     * 查询短信验证码
     * 
     * @param id 短信验证码ID
     * @return 短信验证码
     */
    @Override
    public SmsCode selectSmsCodeById(Long id)
    {
        return smsCodeMapper.selectSmsCodeById(id);
    }


    /**
     * 查询短信验证码列表
     *
     * @param smsCode 短信验证码
     * @return 短信验证码
     */
    @Override
    public Pager<SmsCodeDto> selectSmsCodePager(SmsCode smsCode, Integer page, Integer limit, String sort, String order)
    {
        Pager<SmsCodeDto> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(smsCode);

        List<SmsCode> smsCodeList=smsCodeMapper.selectSmsCodePager(pager);
        List<SmsCodeDto> dtolsit = new ArrayList<SmsCodeDto>();
        smsCodeList.forEach(smsCodes -> {
            SmsCodeDto smsCodeDto = new SmsCodeDto();
            smsCodeDto.setId(smsCodes.getId());
            smsCodeDto.setInfoType(smsCodes.getInfoType());
            smsCodeDto.setUserId(smsCodes.getUserId());
            smsCodeDto.setUserName(smsCodes.getUserName());
            smsCodeDto.setCode(smsCodes.getCode());
            smsCodeDto.setCodeType(smsCodes.getCodeType());
            smsCodeDto.setReferStatus(smsCodes.getReferStatus());
            smsCodeDto.setCreateTime(smsCodes.getCreateTime().getTime());
            dtolsit.add(smsCodeDto);
        });
        pager.setList(dtolsit);
        pager.setTotal(smsCodeMapper.querySmsCodeTotal(pager));
        return pager;
    }

}
