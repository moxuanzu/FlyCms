package com.flycms.modules.notify.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.SmsApi;
import com.flycms.modules.notify.domain.dto.SmsApiDto;
import com.flycms.modules.notify.domain.po.SmsApiUnionSignPO;

/**
 * 短信接口Service接口
 * 
 * @author admin
 * @date 2020-05-27
 */
public interface ISmsApiService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信接口
     *
     * @param smsApi 短信接口
     * @return 结果
     */
    public int insertSmsApi(SmsApi smsApi);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除短信接口
     *
     * @param ids 需要删除的短信接口ID
     * @return 结果
     */
    public int deleteSmsApiByIds(Long[] ids);

    /**
     * 删除短信接口信息
     *
     * @param id 短信接口ID
     * @return 结果
     */
    public int deleteSmsApiById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信接口
     *
     * @param smsApi 短信接口
     * @return 结果
     */
    public int updateSmsApi(SmsApi smsApi);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询短信接口
     * 
     * @param id 短信接口ID
     * @return 短信接口
     */
    public SmsApi selectSmsApiById(Long id);

    /**
     * 按模板id查询手机短信模板信息
     *
     * @param tpId
     * @return
     */
    public SmsApiUnionSignPO findSmsApiUnionSignByTpId(Long tpId);

    /**
     * 查询短信接口列表
     * 
     * @param smsApi 短信接口
     * @return 短信接口集合
     */
    public Pager<SmsApiDto> selectSmsApiPager(SmsApi smsApi, Integer page, Integer limit, String sort, String order);
}
