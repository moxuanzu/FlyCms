package com.flycms.modules.data.domain.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 标签数据传输对象 fly_label
 * 
 * @author admin
 * @date 2020-11-18
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class LabelDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 标签名称 */
    @Excel(name = "标签名称")
    private String title;
    /** 标签说明 */
    @Excel(name = "标签说明")
    private String content;
    /** 使用数量 */
    @Excel(name = "话题使用数量")
    private Integer countTopic;
    /** 使用数量 */
    @Excel(name = "小组使用数量")
    private Integer countGroup;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

}
