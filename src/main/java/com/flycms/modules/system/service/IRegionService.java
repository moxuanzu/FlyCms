package com.flycms.modules.system.service;

import java.util.List;

import com.flycms.modules.system.domain.Region;
import com.flycms.modules.system.domain.dto.RegionDTO;

/**
 * 行政区域Service接口
 * 
 * @author admin
 * @date 2020-05-31
 */
public interface IRegionService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增行政区域
     *
     * @param region 行政区域
     * @return 结果
     */
    public int insertRegion(Region region);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除行政区域
     *
     * @param ids 需要删除的行政区域ID
     * @return 结果
     */
    public int deleteRegionByIds(Long[] ids);

    /**
     * 删除行政区域信息
     *
     * @param id 行政区域ID
     * @return 结果
     */
    public int deleteRegionById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改行政区域
     *
     * @param region 行政区域
     * @return 结果
     */
    public int updateRegion(Region region);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验行政区域名称是否唯一
     *
     * @param region 行政区域
     * @return 结果
     */
    public String checkRegionRegionNameUnique(Region region);


    /**
     * 查询行政区域
     * 
     * @param id 行政区域ID
     * @return 行政区域
     */
    public Region selectRegionById(Long id);

    /**
     * 查询行政区域所有列表
     *
     * @param region 行政区域
     * @return 行政区域集合
     */
    public List<RegionDTO> selectRegionList(Region region);
}
