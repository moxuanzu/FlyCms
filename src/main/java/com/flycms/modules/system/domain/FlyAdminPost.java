package com.flycms.modules.system.domain;

import lombok.Data;

/**
 * 用户和岗位关联 fly_admin_post
 * 
 * @author kaifei sun
 */
@Data
public class FlyAdminPost
{
    /** 用户ID */
    private Long adminId;
    
    /** 岗位ID */
    private Long postId;

}
