package com.flycms.modules.elastic.directive;

import com.flycms.framework.web.tag.BaseTag;
import com.flycms.modules.elastic.service.IElasticSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class SearchDirective extends BaseTag {

    @Autowired
    private IElasticSearchService elasticSearchService;;

    public SearchDirective() {
        super(SearchDirective.class.getName());
    }

    public Object paginate(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        String title = getParam(params, "title");
        return elasticSearchService.searchDocument(pageNum, pageSize, title, "title", "content");
    }
}
