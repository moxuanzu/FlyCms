package com.flycms.modules.elastic.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.elastic.domain.dto.SearchDTO;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentBuilder;

import java.util.List;
import java.util.Map;

public interface IElasticSearchService {
    //检查链接是否存在
    public RestHighLevelClient instance();

    // 创建索引
    public boolean createIndex(String type, XContentBuilder mappingBuilder);


    // 删除所有索引
    public boolean deleteAllIndex();

    // 创建指定文档
    public void createDocument(String type, String id, Map<String, Object> source);

    // 更新指定文档
    public void updateDocument(String type, String id, Map<String, Object> source);

    // 根据节点和id删除文档
    public void deleteDocument(String type, String id);

    // 批量创建文档
    public void bulkDocument(String type, Map<String, Map<String, Object>> sources);

    // 批量删除文档
    public void bulkDeleteDocument(String type, Long[] ids);

    /**
     * 翻页查询
     *
     * @param pageNo
     * @param pageSize
     * @param keyword  要查询的内容
     * @param fields   要查询的字段，可以为多个
     * @return 分页对象 {@link Pager}
     */
    public Pager<SearchDTO> searchDocument(Integer pageNo, Integer pageSize, String keyword, String... fields);
}
