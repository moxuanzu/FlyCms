package com.flycms.modules.elastic.domain.dto;

import lombok.Data;

@Data
public class SearchDTO {
    /** ID */
    private String id;
    /** 标题 */
    private String title;
    /** 内容 */
    private String content;
}
