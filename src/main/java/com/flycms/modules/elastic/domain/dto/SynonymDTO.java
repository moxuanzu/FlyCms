package com.flycms.modules.elastic.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 同义词词库数据传输对象 fly_synonym
 * 
 * @author admin
 * @date 2020-10-22
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SynonymDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 同义词 */
    @Excel(name = "同义词")
    private String synonymWord;
}
