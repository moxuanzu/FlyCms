package com.flycms.modules.site.service;

import com.flycms.modules.site.domain.Site;

/**
 * 官网设置Service接口
 * 
 * @author admin
 * @date 2020-07-08
 */
public interface ISiteService
{

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改官网设置
     *
     * @param site 官网设置
     * @return 结果
     */
    public int updateSite(Site site);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询官网设置
     * 
     * @return 官网设置
     */
    public Site selectSite();
    
}
