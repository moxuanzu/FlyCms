package com.flycms.modules.tool.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.tool.domain.IpAddress;
import com.flycms.modules.tool.domain.dto.IpAddressDTO;

import java.util.List;

/**
 * IP地址库Service接口
 * 
 * @author admin
 * @date 2020-12-10
 */
public interface IIpAddressService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增IP地址库
     *
     * @param ipAddress IP地址库
     * @return 结果
     */
    public int insertIpAddress(IpAddress ipAddress);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除IP地址库
     *
     * @param ids 需要删除的IP地址库ID
     * @return 结果
     */
    public int deleteIpAddressByIds(Long[] ids);

    /**
     * 删除IP地址库信息
     *
     * @param id IP地址库ID
     * @return 结果
     */
    public int deleteIpAddressById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改IP地址库
     *
     * @param ipAddress IP地址库
     * @return 结果
     */
    public int updateIpAddress(IpAddress ipAddress);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验IP段起始是否唯一
     *
     * @param id ID
     * @param start_ip IP段起始
     * @return 结果
     */
    public String checkIpAddressStartIpUnique(Long id,String start_ip);


    /**
     * 查询IP地址库
     * 
     * @param id IP地址库ID
     * @return IP地址库
     */
    public IpAddressDTO findIpAddressById(Long id);

    /**
     * 查询IP地址
     *
     * @param ip IP地址
     * @return IP地址详细信息
     */
    public IpAddressDTO findSearchIpAddress(String ip);

    /**
     * 查询IP地址库列表
     * 
     * @param ipAddress IP地址库
     * @return IP地址库集合
     */
    public Pager<IpAddressDTO> selectIpAddressPager(IpAddress ipAddress, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的IP地址库列表
     *
     * @param ipAddress IP地址库
     * @return IP地址库集合
     */
    public List<IpAddressDTO> exportIpAddressList(IpAddress ipAddress);
}
