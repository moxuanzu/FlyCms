package com.flycms.modules.tool.service.impl;


import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.common.utils.ip.IpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.tool.mapper.IpAddressMapper;
import com.flycms.modules.tool.domain.IpAddress;
import com.flycms.modules.tool.domain.dto.IpAddressDTO;
import com.flycms.modules.tool.service.IIpAddressService;

import java.util.ArrayList;
import java.util.List;

/**
 * IP地址库Service业务层处理
 * 
 * @author admin
 * @date 2020-12-10
 */
@Service
public class IpAddressServiceImpl implements IIpAddressService 
{
    @Autowired
    private IpAddressMapper ipAddressMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增IP地址库
     *
     * @param ipAddress IP地址库
     * @return 结果
     */
    @Override
    public int insertIpAddress(IpAddress ipAddress)
    {
        ipAddress.setId(SnowFlakeUtils.nextId());
        return ipAddressMapper.insertIpAddress(ipAddress);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除IP地址库
     *
     * @param ids 需要删除的IP地址库ID
     * @return 结果
     */
    @Override
    public int deleteIpAddressByIds(Long[] ids)
    {
        return ipAddressMapper.deleteIpAddressByIds(ids);
    }

    /**
     * 删除IP地址库信息
     *
     * @param id IP地址库ID
     * @return 结果
     */
    @Override
    public int deleteIpAddressById(Long id)
    {
        return ipAddressMapper.deleteIpAddressById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改IP地址库
     *
     * @param ipAddress IP地址库
     * @return 结果
     */
    @Override
    public int updateIpAddress(IpAddress ipAddress)
    {
        return ipAddressMapper.updateIpAddress(ipAddress);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验IP段起始是否唯一
     *
     * @param id ID
     * @param startIp IP段起始
     * @return 结果
     */
     @Override
     public String checkIpAddressStartIpUnique(Long id,String startIp)
     {
         IpAddress ipAddress = new IpAddress();
         ipAddress.setId(id);
         ipAddress.setStartIp(startIp);
         int count = ipAddressMapper.checkIpAddressStartIpUnique(ipAddress);
         if (count > 0){
             return UserConstants.NOT_UNIQUE;
         }
         return UserConstants.UNIQUE;
     }


    /**
     * 查询IP地址库
     * 
     * @param id IP地址库ID
     * @return IP地址库
     */
    @Override
    public IpAddressDTO findIpAddressById(Long id)
    {
        IpAddress ipAddress=ipAddressMapper.findIpAddressById(id);
        return BeanConvertor.convertBean(ipAddress,IpAddressDTO.class);
    }

    /**
     * 查询IP地址
     *
     * @param ip IP地址
     * @return IP地址详细信息
     */
    @Override
    public IpAddressDTO findSearchIpAddress(String ip)
    {
        if(StrUtils.isEmpty(ip)){
            return null;
        }
        IpAddress ipAddress=ipAddressMapper.findSearchIpAddress(IpUtils.ip2Long(ip));
        return BeanConvertor.convertBean(ipAddress,IpAddressDTO.class);
    }


    /**
     * 查询IP地址库列表
     *
     * @param ipAddress IP地址库
     * @return IP地址库
     */
    @Override
    public Pager<IpAddressDTO> selectIpAddressPager(IpAddress ipAddress, Integer page, Integer limit, String sort, String order)
    {
        Pager<IpAddressDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(ipAddress);

        List<IpAddress> ipAddressList=ipAddressMapper.selectIpAddressPager(pager);
        List<IpAddressDTO> dtolsit = new ArrayList<IpAddressDTO>();
        ipAddressList.forEach(entity -> {
            IpAddressDTO dto = new IpAddressDTO();
            dto.setId(entity.getId());
            dto.setStartIp(entity.getStartIp());
            dto.setEndIp(entity.getEndIp());
            dto.setCountry(entity.getCountry());
            dto.setProvince(entity.getProvince());
            dto.setCity(entity.getCity());
            dto.setCounty(entity.getCounty());
            dto.setAddress(entity.getAddress());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(ipAddressMapper.queryIpAddressTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的IP地址库列表
     *
     * @param ipAddress IP地址库
     * @return IP地址库集合
     */
    @Override
    public List<IpAddressDTO> exportIpAddressList(IpAddress ipAddress) {
        return BeanConvertor.copyList(ipAddressMapper.exportIpAddressList(ipAddress),IpAddressDTO.class);
    }
}
