package com.flycms.modules.fullname.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.fullname.domain.SearchKeyword;
import com.flycms.modules.fullname.domain.dto.SearchKeywordDTO;

/**
 * 姓名搜索Service接口
 * 
 * @author admin
 * @date 2020-10-13
 */
public interface ISearchKeywordService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增姓名搜索
     *
     * @param searchKeyword 姓名搜索
     * @return 结果
     */
    public int insertSearchKeyword(SearchKeyword searchKeyword);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除姓名搜索
     *
     * @param ids 需要删除的姓名搜索ID
     * @return 结果
     */
    public int deleteSearchKeywordByIds(Long[] ids);

    /**
     * 删除姓名搜索信息
     *
     * @param id 姓名搜索ID
     * @return 结果
     */
    public int deleteSearchKeywordById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改姓名搜索
     *
     * @param searchKeyword 姓名搜索
     * @return 结果
     */
    public int updateSearchKeyword(SearchKeyword searchKeyword);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验姓名是否唯一
     *
     * @param searchKeyword 姓名搜索
     * @return 结果
     */
    public String checkSearchKeywordFullNameUnique(SearchKeyword searchKeyword);


    /**
     * 查询姓名搜索
     * 
     * @param id 姓名搜索ID
     * @return 姓名搜索
     */
    public SearchKeywordDTO findSearchKeywordById(Long id);

    /**
     * 查询姓名搜索列表
     * 
     * @param searchKeyword 姓名搜索
     * @return 姓名搜索集合
     */
    public Pager<SearchKeywordDTO> selectSearchKeywordPager(SearchKeyword searchKeyword, Integer page, Integer limit, String sort, String order);
}
