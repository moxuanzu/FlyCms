package com.flycms.modules.fullname.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

/**
 * 姓氏数据传输对象 fly_surname
 * 
 * @author admin
 * @date 2020-10-13
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SurnameDTO
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 姓氏 */
    @Excel(name = "姓氏")
    private String lastName;
}
