package com.flycms.framework.initialize;

import com.flycms.common.utils.StrUtils;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateModelException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 标签解析抽象类
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 14:14 2018/7/8
 */

@Service
public abstract class TagsPlugin extends ApplicationObjectSupport implements TemplateDirectiveModel {
	@Resource
	protected HttpServletRequest request;

	@Resource
	private FreeMarkerConfigurer freeMarkerConfigurer;

	@PostConstruct
	public void init() throws TemplateModelException {
		String className = this.getClass().getName().substring(this.getClass().getName().lastIndexOf(".") + 1);
		String beanName = StringUtils.uncapitalize(className);
		String tagName = "fly_" + StrUtils.toUnderline(beanName);
		freeMarkerConfigurer.getConfiguration().setSharedVariable(tagName, this.getApplicationContext().getBean(beanName));
	}

}
