$(document).ready(function(){
    //===========================================登录开始=================================================

    $(document).on('click', '#comm-submit', function (){
        var username=$("#username").val();
        var password=$("#password").val();
        var redirectUrl=$("#redirectUrl").val();
        console.log(username,password,redirectUrl);
        $.ajax({
            url:'/user/login',
            type:'post',
            data:{'username':username,'password':password,"redirectUrl":redirectUrl},
            dataType:'json',
            success:function(data){
                if(data.code==200){
                    layer.msg("登录成功", { shift: -1 }, function () {
                        location.href = data.data;
                    });
                }else{
                    alert(data.msg);
                }
            },
            error:function(){
                console.log('请求出错！');
            }
        })
    });

    $("#comm-submit").keydown(function(e){
        var e = e || event,
            keycode = e.which || e.keyCode;
        if (keycode==13) {
            $("#comm-submit").trigger("click");
        }
    });


    $('#verify_code').on("click",function(event){
        var time=$("#verifytime").val();
        var email = $("#email").val();
        if($("#email").val()==""){
            $("#email").removeClass('has-success').addClass('is-invalid');
            return false;
        }
        if(time=="" || time==0){
            var count = 60;
            var countdown = setInterval(CountDown, 1000);
            function CountDown() {
                $("#verifytime").val(count);
                $("#verify_code").html(count + " 秒后");
                if (count == 0) {
                    $("#verify_code").html("发送验证码");
                    clearInterval(countdown);
                }
                count--;
            }
            if(email.length>0){
                if(/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/.test(email)){
                    $("#email").removeClass('is-invalid').addClass('has-success');
                }else{
                    $("#email").removeClass('has-success').addClass('is-invalid');
                }
            }else{
                $("#email").removeClass('has-success').addClass('is-invalid');
            }
            $.ajax({
                url: "/register/email?"+Math.random(),
                data: {"username":email},
                dataType: "json",
                type :  "POST",
                cache : false,
                async: false,
                error : function(i, g, h) {
                    layer.msg('发送错误', {icon: 2});
                },
                success: function(data){
                    if(data.code==200){
                        layer.msg('已发送，请注意查收！', {icon: 1});
                        return false;
                    } else {
                        layer.msg(data.msg, {icon: 5});
                        return false;
                    }
                }
            });
            return false;
        }else{
            layer.msg(time+'后再试', {icon: 2});
            return false;
        }
    });

    //手机用户注册提交
    $("#reg-submit").on("click", function(){
        var email=$("#email").val();
        var code=$("#reg_code").val();
        var password=$("#password").val();
        var password2=$("#password2").val();
        var invite=$("#invite").val();
        var captcha=$("#captcha").val();
        if(email.length>0){
            if(!/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/.test(email)){
                $("#email").removeClass('has-success').addClass('is-invalid');
                $("#email").focus();
                return false;
            }else{
                $("#email").removeClass('is-invalid').addClass('has-success');
            }
        }else{
            $("#email").removeClass('has-success').addClass('is-invalid');
            $("#email").focus();
            return false;
        }

        if(code.length>0){
            if(!/^[0-9a-zA-Z]{4}$/.test(code)){
                $("#reg_code").removeClass('has-success').addClass('is-invalid');
                $("#reg_code").focus();
                return false;
            }else{
                $("#reg_code").removeClass('is-invalid').addClass('has-success');
            }
        }else{
            $("#reg_code").removeClass('has-success').addClass('is-invalid');
            $("#reg_code").focus();
            return false;
        }

        if(password.length>=6){
            if(!/^[0-9a-zA-Z_~!@#$%^&*()_+]{6,20}$/.test(password)){
                $("#password").removeClass('has-success').addClass('is-invalid');
                $("#password").focus();
                return false;
            }else{
                $("#password").removeClass('is-invalid').addClass('has-success');
            }
        }else{
            $("#password").removeClass('has-success').addClass('is-invalid');
            $("#password").focus();
            return false;
        }

        if(password!=password2){
            $("#password").removeClass('has-success').addClass('is-invalid');
            $("#password").focus();
            return false;
        }

        jQuery.ajax({
            url: "/register/add/email",
            data: {"username":email,"code":code,"password":password,"password2":password2,"invite":invite,"captcha":captcha},
            dataType: "json",
            type :  "POST",
            cache : false,
            async: false,
            error : function(i, g, h) {
                layer.msg('发送错误', {icon: 2});
            },
            success : function(data) {
                if(data.code==200){
                    window.location.href = "/user/my";
                    return false;
                }else{
                    layer.msg(data.msg, {icon: 2});
                    return false;
                }
            }
        });
        return false;
    });
});